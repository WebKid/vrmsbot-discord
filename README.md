# Virtual Stallman bot for Discord

A virtual Stallman for your Discord server.



## Manual

### Requirements

- Python 3.4.2+
- python3-pip
- Requirements installed

### Setup

```bash
python3 -m pip install -U -r requirements.txt
python3 setup.py
```

Info: First, execute the `setup.py` script and enter your generated bot token from <https://discordapp.com/developers/applications/>.

### Execution

```bash
python3 run.py
```



## License

AGPLv3+. Please help others the way you were helped, and share your code changes with the world!

Have fun!



**Note**

I want to sincerely apologize to Dr. Stallman for Discord’s proprietary nature.